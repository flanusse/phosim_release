//-*-mode:c++; mode:font-lock;-*-
///
/// @package phosim
/// @file azelradecxy.h
/// @brief General conversion class between camera coords (X,Y) to Az,El to RA Dec
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author 
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
#ifndef AZElRADECXY_H
#define AZElRADECXY_H

#include "erfa.h"
#include "erfam.h"
#include "pal.h"
#include "palmac.h"

#include <cmath>
#include <iostream>
#include <sstream>

//
class AzElRADecXY
{
	private:
		double fEastLongitude;
		double fLatitude;
		double fRASource2000;
		double fDecSource2000;
		double fCosLatitude;
		double fSinLatitude;
		
	public:
		AzElRADecXY(double EastLongitude, double Latitude);
		AzElRADecXY(double EastLongitude, double Latitude, double RASource2000,
					  double DecSource2000);
		virtual ~AzElRADecXY();
                
                void SetLocation(double EastLongitude, double Latitude);
    
                void XY2RADec2000(double X, double Y, double MJD, double& RA2000,
						  double& Dec2000);

		void XY2RADec2000(double X, double Y, double MJD, 
				  double RASource2000, double DecSource2000,
				  double& RA2000, double& Dec2000);

		void AzEl2RADec2000(double Az, double El, double MJD, double& RA2000, double& Dec2000);
		void RADec2000ToAzEl(double fRA2000, double fDec2000, double MJD,
							 double& fSourceAz, double& fSourceElev);
		void RADec2000ToXY(double RA2000, double Dec2000, double MJD, double& X,
						   double& Y);
		void RADec2000ToXY(double RA2000, double Dec2000, double MJD,
						   double RASource2000, double DecSource2000, double& X,
						   double& Y);
						   
		void AzElToXY(double Az, double El, double MJD, double& X, double& Y);
		void AzElToXY(double Az, double El, double MJD, double RASource2000, double DecSource2000, double& X, double& Y);
		void AzElToXY(double Az_Rad, double El_Rad, double TrackingAz_Rad, double TrackingEl_Rad, double& X_Deg, double& Y_Deg);
		void XYToAzEl(double X_Deg, double Y_Deg, double TrackingAz_Rad, double TrackingEl_Rad, double& Az_Rad, double& El_Rad);
		
		void XYToDlDmDn(double X, double Y, double fAzSrc, double fElSrc,
						double& Dl, double& Dm, double& Dn);
		void DlDmDnToXY(double Dl, double Dm, double Dn, double fAzSrc,
						double fElSrc, double& fX, double& fY);
		void DlDmDnToAzEl(double Dl, double Dm, double Dn, double& fAz,
						  double& fEl);
						  
		void Derotate(double MJD, double X, double Y,  double RASource2000,
					  double DecSource2000, double& XDeRotated, double& YDerotated);
		void Derotate(double MJD, double X, double Y, double& XDeRotated,
					  double& YDerotated);
					  
		void setRASource2000(double RA)
		{
			fRASource2000 = RA;
			return;
		};
		void setDecSource2000(double Dec)
		{
			fDecSource2000 = Dec;
			return;
		};
		std::string RAToString(double RA);
		std::string DecToString(double Dec);
};
#endif
