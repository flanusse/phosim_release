;;
;; @package phosim
;; @file validation_4D.pro
;; @brief validation task 4D
;;
;; @brief Created by:
;; @author John R. Peterson (Purdue)
;;
;; @brief Modified by:
;;
;; @warning This code is not fully validated
;; and not ready for full release.  Please
;; treat results with caution.
;;

pro validation_4D,nnn,vers,value,tolerance_low,tolerance_high,task,name,unit,comparison

  print,'Task 4D'
  !p.multi=[0,2,2]

  ll=0.01+findgen(11)*15.0

  data1=mrdfits('lsst_e_4300_f2_R10_S00_E000.fits.gz',0,/silent)
  image1=rebin(data1(0:3999,0:3999),400,400)
  sss=size(image1)
  xx=(findgen(sss(1))-(sss(1)-2)/2.0)*100.0
  yy=(findgen(sss(2))-(sss(2)-2)/2.0)*100.0
  contour,image1,xx,yy,/fill,xtitle='X Position (microns)',ytitle='Y Position (microns)',/xstyle,/ystyle,nlevels=11,levels=ll
  image1=image1*100.0

  data2=mrdfits('lsst_e_4301_f2_R10_S00_E000.fits.gz',0,/silent)
  image2=rebin(data2(0:3999,0:3999),400,400)
  sss=size(image2)
  xx=(findgen(sss(1))-(sss(1)-2)/2.0)*100.0
  yy=(findgen(sss(2))-(sss(2)-2)/2.0)*100.0
  contour,image2,xx,yy,/fill,xtitle='X Position (microns)',ytitle='Y Position (microns)',/xstyle,/ystyle,nlevels=11,levels=ll
  image2=image2*100.0

  data3=mrdfits('lsst_e_4302_f2_R10_S00_E000.fits.gz',0,/silent)
  image3=rebin(data3(0:3999,0:3999),400,400)
  sss=size(image3)
  xx=(findgen(sss(1))-(sss(1)-2)/2.0)*100.0
  yy=(findgen(sss(2))-(sss(2)-2)/2.0)*100.0
  contour,image3,xx,yy,/fill,xtitle='X Position (microns)',ytitle='Y Position (microns)',/xstyle,/ystyle,nlevels=11,levels=ll
  image3=image3*100.0

  q1=histogram(data1,min=0,bin=5)
  q2=histogram(data2,min=0,bin=5)
  q3=histogram(data3,min=0,bin=5)
  x1=findgen(N_elements(q1))*5.+5./2.
  x2=findgen(N_elements(q2))*5.+5./2.
  x3=findgen(N_elements(q3))*5.+5./2.
  plot,x2,q2,psym=10
  oplot,x1,q1,linestyle=2,psym=10,color=50
  oplot,x3,q3,linestyle=1,psym=10,color=250

  xyouts,0.15,0.9,'Normal Optimization',/norm
  xyouts,0.15,0.45,'Quick Optimization',/norm
  xyouts,0.6,0.9,'Single Photon',/norm

  ss='Background Optimiztion Accuracy'
  xyouts,0.1,0.98,ss,/normal
  ss='Validation Task 4D; '+vers
  xyouts,0.7,0.98,ss,/normal


  f1=total(image1) & f2=total(image2) & f3=total(image3)
  good=where(image1 ge 1 or image2 ge 1)
  chi2=1e30
  if N_elements(good) gt 1 then begin
     chi2=total((image1(good)/f1-image2(good)/f2)^2/((image1(good)/f1/f1+image2(good)/f2/f2)))/float(N_elements(good))
  endif
  name(nnn,0)='Normal Opt Relative Pixel'
  value(nnn,0)=chi2
  tolerance_low(nnn,0)=0.0
  tolerance_high(nnn,0)=2.0
  unit(nnn,0)=' (!4V!3!U2!N/dof)'
  comparison(nnn,0)='Exact Calculation'

  name(nnn,1)='Normal Opt Absolute Pixel'
  value(nnn,1)=abs(f1-f2)/f2*100.0
  tolerance_low(nnn,1)=0.0
  tolerance_high(nnn,1)=10.0
  unit(nnn,1)=' %'
  comparison(nnn,1)='Exact Calculation'

  good=where(image3 ge 1 or image2 ge 1)
  chi2=1e30
  if N_elements(good) gt 1 then begin
     chi2=total((image3(good)/f3-image2(good)/f2)^2/((image3(good)/f3/f3+image2(good)/f2/f2)))/float(N_elements(good))
  endif
  name(nnn,2)='Quick Opt Relative Pixel'
  value(nnn,2)=chi2
  tolerance_low(nnn,2)=0.0
  tolerance_high(nnn,2)=3.0
  unit(nnn,2)=' (!4V!3!U2!N/dof)'
  comparison(nnn,2)='Exact Calculation'

  name(nnn,3)='Quick Opt Absolute Pixel'
  value(nnn,3)=abs(f3-f2)/f2*100.0
  tolerance_low(nnn,3)=0.0
  tolerance_high(nnn,3)=10.0
  unit(nnn,3)=' %'
  comparison(nnn,3)='Exact Calculation'

  task(nnn,0)='4D Corner chip Back Opt on/off'

END
